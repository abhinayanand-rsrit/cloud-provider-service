package com.rdops.microservices.cloudproviderservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Update;
import com.rdops.microservices.cloudproviderservice.model.Counter;
import org.springframework.stereotype.Service;

@Service
public class CounterService {

	@Autowired
	private MongoOperations mongo;
	
	public int getNextSequence(String collectionName) {
		
		//get sequence id
		Query query = new Query(Criteria.where("_id").is(collectionName));
		
		//increase sequence id by 1
		Update update = new Update();
		update.inc("seq", 1);
		
		//return new increased id
		FindAndModifyOptions options = new FindAndModifyOptions();
		options.returnNew(true);
		
	    Counter counter = mongo.findAndModify(query, update, options, Counter.class);
	    
	    if (counter == null) {
	    	System.out.println("Unable to get sequence id for collectionName : " + collectionName);
		}
	       
	    return counter.getSeq();
	  }
}
