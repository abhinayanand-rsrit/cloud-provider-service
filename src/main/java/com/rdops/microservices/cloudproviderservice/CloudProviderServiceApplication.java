package com.rdops.microservices.cloudproviderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudProviderServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudProviderServiceApplication.class, args);
	}
}
