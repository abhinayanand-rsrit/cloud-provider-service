package com.rdops.microservices.cloudproviderservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.rdops.microservices.cloudproviderservice.model.CloudProvider;

public interface CloudProviderRepository extends MongoRepository<CloudProvider, String>{
	
	public CloudProvider findBy_id(ObjectId _id);
	
	@Query("{'$and':[ {'accountId':?0}, {'cloudEnvName':?1} ] }")
	public Iterable<CloudProvider> findByAccountIdAndCloudEnvName(int accountId, String cloudEnvName);
}
