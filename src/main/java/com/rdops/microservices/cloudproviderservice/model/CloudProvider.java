package com.rdops.microservices.cloudproviderservice.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="cloud_envs")
public class CloudProvider {
	
	@Id
	private ObjectId _id;
	
	@Field(value="cloudprovider_Id")
	private int cloudProviderId;
	
	@Field(value="account_Id")
	private int accountId;
	
	@Field(value="CloudEnv_name")
	private String cloudEnvName;
	
	private String environment;
	
	@Field(value="AWSAccessKey")
	private String aWSAccessKey;
	
	@Field(value="AWSSecretKey")
	private String aWSSecretKey;
	
	@Field(value="Subscription_Id")
	private String subscriptionId;
	
	@Field(value="Client_Id")
	private String clientId;
	
	@Field(value="Client_Secret")
	private String clientSecret;
	
	@Field(value="Tenant_Id")
	private String tenantId;
	
	@Field(value="Controller_Ip")
	private String controllerIp;
	
	@Field(value="Openstack_username")
	private String openstackUsername;
	
	@Field(value="Openstack_Password")
	private String openstackPassword;
	
	@Field(value="vcenter_serverip")
	private String vcenterServerip;
	
	@Field(value="Vcenter_username")
	private String vcenterUsername;
	
	@Field(value="Vcenter_password")
	private String vcenterPassword;
	
	@Field(value="created_at")
	public Date createdAt;
	
	@Field(value="updated_at")
	public Date updatedAt;
	
	public CloudProvider() {
		
	}

	public CloudProvider(ObjectId _id, int cloudProviderId, int accountId, String cloudEnvName, String environment,
			String aWSAccessKey, String aWSSecretKey, String subscriptionId, String clientId, String clientSecret,
			String tenantId, String controllerIp, String openstackUsername, String openstackPassword,
			String vcenterServerip, String vcenterUsername, String vcenterPassword, int createdById, Date createdAt,
			Date updatedAt) {
		super();
		this._id = _id;
		this.cloudProviderId = cloudProviderId;
		this.accountId = accountId;
		this.cloudEnvName = cloudEnvName;
		this.environment = environment;
		this.aWSAccessKey = aWSAccessKey;
		this.aWSSecretKey = aWSSecretKey;
		this.subscriptionId = subscriptionId;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.tenantId = tenantId;
		this.controllerIp = controllerIp;
		this.openstackUsername = openstackUsername;
		this.openstackPassword = openstackPassword;
		this.vcenterServerip = vcenterServerip;
		this.vcenterUsername = vcenterUsername;
		this.vcenterPassword = vcenterPassword;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public int getCloudProviderId() {
		return cloudProviderId;
	}

	public void setCloudProviderId(int cloudProviderId) {
		this.cloudProviderId = cloudProviderId;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getCloudEnvName() {
		return cloudEnvName;
	}

	public void setCloudEnvName(String cloudEnvName) {
		this.cloudEnvName = cloudEnvName;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getaWSAccessKey() {
		return aWSAccessKey;
	}

	public void setaWSAccessKey(String aWSAccessKey) {
		this.aWSAccessKey = aWSAccessKey;
	}

	public String getaWSSecretKey() {
		return aWSSecretKey;
	}

	public void setaWSSecretKey(String aWSSecretKey) {
		this.aWSSecretKey = aWSSecretKey;
	}

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getControllerIp() {
		return controllerIp;
	}

	public void setControllerIp(String controllerIp) {
		this.controllerIp = controllerIp;
	}

	public String getOpenstackUsername() {
		return openstackUsername;
	}

	public void setOpenstackUsername(String openstackUsername) {
		this.openstackUsername = openstackUsername;
	}

	public String getOpenstackPassword() {
		return openstackPassword;
	}

	public void setOpenstackPassword(String openstackPassword) {
		this.openstackPassword = openstackPassword;
	}

	public String getVcenterServerip() {
		return vcenterServerip;
	}

	public void setVcenterServerip(String vcenterServerip) {
		this.vcenterServerip = vcenterServerip;
	}

	public String getVcenterUsername() {
		return vcenterUsername;
	}

	public void setVcenterUsername(String vcenterUsername) {
		this.vcenterUsername = vcenterUsername;
	}

	public String getVcenterPassword() {
		return vcenterPassword;
	}

	public void setVcenterPassword(String vcenterPassword) {
		this.vcenterPassword = vcenterPassword;
	}


	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public String toString() {
		return "[_id=" + _id + ", cloudProviderId=" + cloudProviderId + ", accountId=" + accountId
				+ ", cloudEnvName=" + cloudEnvName + ", environment=" + environment + ", AWSAccessKey=" + aWSAccessKey
				+ ", AWSSecretKey=" + aWSSecretKey + ", subscriptionId=" + subscriptionId + ", clientId=" + clientId
				+ ", clientSecret=" + clientSecret + ", tenantId=" + tenantId + ", controllerIp=" + controllerIp
				+ ", openstackUsername=" + openstackUsername + ", openstackPassword=" + openstackPassword
				+ ", vcenterServerip=" + vcenterServerip + ", VcenterUsername=" + vcenterUsername + ", VcenterPassword="
				+ vcenterPassword + ", createdAt=" + createdAt + ", updatedAt="
				+ updatedAt + "]";
	}
	
	public JSONObject toObject() {
		
		JSONObject jsondata = new JSONObject();
		jsondata.put("_id", this._id);
		jsondata.put("cloudProvider_Id",this.cloudProviderId);
		jsondata.put("account_Id",this.accountId);
		jsondata.put("CloudEnv_name",this.cloudEnvName);
		jsondata.put("environment",this.environment);
		jsondata.put("AWSAccessKey",this.aWSAccessKey);
		jsondata.put("AWSSecretKey",this.aWSSecretKey);
		jsondata.put("Subscription_Id",this.subscriptionId);
		jsondata.put("Client_Id",this.clientId);
		jsondata.put("Client_Secret",this.clientSecret);
		jsondata.put("Tenant_Id",this.tenantId);
		jsondata.put("Controller_Ip",this.controllerIp);
		jsondata.put("Openstack_username",this.openstackUsername);
		jsondata.put("Openstack_Password",this.openstackPassword);
		jsondata.put("vcenter_serverip",this.vcenterServerip);
		jsondata.put("Vcenter_username",this.vcenterUsername);
		jsondata.put("Vcenter_password",this.vcenterPassword);
		jsondata.put("created_at",this.createdAt);
		jsondata.put("updated_at",this.updatedAt);
		return jsondata;
	}
}
