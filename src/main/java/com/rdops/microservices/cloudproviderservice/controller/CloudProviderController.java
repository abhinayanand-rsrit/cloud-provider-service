package com.rdops.microservices.cloudproviderservice.controller;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.rdops.microservices.cloudproviderservice.model.CloudProvider;
import com.rdops.microservices.cloudproviderservice.service.CounterService;
import com.rdops.microservices.cloudproviderservice.repository.CloudProviderRepository;

@RestController
public class CloudProviderController {
	
	@Autowired
	private CloudProviderRepository cloudProviderRepository;
	
	@Autowired
	private CounterService counterService;
	
	@GetMapping("/cloud-providers")
	public String getAllCloudProvider() {
		
		JSONObject jsondata = new JSONObject();
		
		try {
			List<CloudProvider> cloudProvidersList =  cloudProviderRepository.findAll();
			jsondata.put("status", 1);
			jsondata.put("cloudproviders", cloudProvidersList);
		} catch (Exception e) {
			jsondata.put("status", 0);
			jsondata.put("message", "Error while fetching cloud providers list");
		}
		
		return jsondata.toString();
	}
	
	@PostMapping("/cloud-providers/check-cloud-provider-name")
	public String checkCloudProviderName(@RequestBody ObjectNode body) {
		
		System.out.println(body);
		Iterable<CloudProvider> cloudProviderData= cloudProviderRepository.findByAccountIdAndCloudEnvName(body.get("accountId").asInt(), body.get("cloudEnvName").asText());
		System.out.println(cloudProviderData);
		return "";
	}
	
	@PostMapping("/cloud-providers")
	public String saveCloudProvider(@RequestBody CloudProvider cloudProvider) {
		
		JSONObject jsondata = new JSONObject();
		
		cloudProvider.set_id(ObjectId.get());
		cloudProvider.setCloudProviderId(counterService.getNextSequence("cloud_envs"));
		cloudProvider.setCreatedAt(new Date());
		cloudProvider.setUpdatedAt(new Date());
		CloudProvider savedcloudProvider = cloudProviderRepository.save(cloudProvider);
		jsondata.put("status", 1);
		jsondata.put("env_data", savedcloudProvider.toObject());
		
		return jsondata.toString();
	}
	
	@DeleteMapping("/cloud-providers")
	public String deleteUserById(@RequestBody Map<String,ObjectId> Body) {
		cloudProviderRepository.delete(cloudProviderRepository.findBy_id(Body.get("_id")));
		JSONObject jsondata = new JSONObject();
		jsondata.put("status", 1);
		jsondata.put("message", "Deleted successfully");
		
		return jsondata.toString();
	}
}
